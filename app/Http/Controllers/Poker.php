<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

class Poker extends Controller
{
	public $maxCant;
	public $minCant;
    /**
     * Instantiate a new controller instance.
     *
     * @return
     */
    public function __construct()
    {
    	// Max and Min cant of cards
    	$this->maxCant=7;
    	$this->minCant=5;
    }

	/**
     * 
     * @return 
     */
    public function index()
    {
    	$result=$this->isStraight([2,3,4,13,12,11,14]);
    	print_r($result);
    }


    /**
     * @param array $cards
     * @return bool
     */
    public function isStraight(array $cards)
    {
    	//return true;
        if(!$this->validateCards($cards)) {
            return false;
        }
        if(!$this->isConsecutive($cards)) {
            return false;
        }
        return true;
    }

    /**
     * @param array $cards
     * @return bool
     */
    private function validateCards(array $cards)
    {
    	// Validate cant card between 5 and 7
        $cant = count($cards);
        if($cant < $this->minCant || $cant > $this->maxCant) {
            return false;
        }
    	foreach ($cards as $key => $value) {
    		if($value<2 || $value>14){
    			return false;
    		}
    	}
        return true;
    	
    }

    /**
     * @param array $cards
     * @return bool
     */
    private function isConsecutive(array $cards)
    {
    	//order array min to max number
        sort($cards);
       
        $consecutive = array();
        $i=0;
        foreach ($cards as $key => $value) {
        	//validate if exit in to aaray a prev number or next number consecutive
        	if (array_search($value + 1, $cards) !== false || array_search($value - 1, $cards) !== false) {
        		//validate if not exit prev number in to new array of consecutives 
        	 	if(isset($consecutive[$key - 1]) && !empty($consecutive[$key - 1]) && $consecutive[$key - 1] != $value - 1) {
                    $consecutive = [];
                }

        		$consecutive[]=$value;
        	}
        	// validate if number 14 exist and if the min number is 2 for convert 14 to 1
        	if ($value == 14 && min($consecutive) == 2 && max($consecutive) < 13) {
                $consecutive[] = $value;
            }
            $i++;
        }

        //validate if count consecutives is 5
        if(count($consecutive) < $this->minCant){
        	return false;
        }

        print_r($consecutive);

        return true;
    } 

}


	
   
  






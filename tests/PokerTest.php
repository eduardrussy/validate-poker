<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use App\Http\Controllers\Poker;

class PokerTest extends TestCase
{

    public function testAlgorithm() {
    	$poker = new Poker();

		$results1 = $poker->isStraight([2, 3, 4 ,5, 6]);
		$this->assertEquals($results1, true, "2, 3, 4 ,5, 6");

		$results2 = $poker->isStraight([14, 5, 4 ,2, 3]);
		$this->assertEquals($results2, true, "14, 5, 4 ,2, 3");

		$results3 = $poker->isStraight([7, 7]);
		$this->assertEquals($results3, false, "7, 7");

		$results4 = $poker->isStraight([1, 5, 4 ,2, 3]);
		$this->assertEquals($results4, false, "1, 5, 4 ,2, 3");

		$results5 = $poker->isStraight([6, 5, 4 ,2, 3, 14]);
		$this->assertEquals($results5, true, "6, 5, 4 ,2, 3, 14");

		$results6 = $poker->isStraight([7, 7, 12 ,11, 3, 4, 14]);
		$this->assertEquals($results6, false, "7, 7, 12 ,11, 3, 4, 14");
		
		$results7 = $poker->isStraight([7, 3, 2]);
		$this->assertEquals($results7, false, "7, 3, 2");

		$results8 = $poker->isStraight([7, 10 ,11, 5, 13, 12, 14]);
		$this->assertEquals($results8, true, "7, 10 ,11, 5, 13, 12, 14");


	}
}
